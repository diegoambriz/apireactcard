import React from 'react';
import SingleCard from "./SingleCard";
import { Card, Button, CardTitle, CardText,CardImg } from 'reactstrap';
class Apicomponent2 extends React.Component {

	constructor(props) {
		super(props);
		this.state = {users: []};
		this.headers = [
			{ key: 'userId', label: 'User ID' },
			{ key: 'id', label: 'ID' },
			{ key: 'title', label: 'Title' },
            { key: 'body', label: 'Body' },
            { key: 'thumbnailUrl', label: 'Thumbnail'}
		];
	}
	
	componentDidMount() {
		fetch('https://jsonplaceholder.typicode.com/photos')
			.then(response => {
				return response.json();
			}).then(result => {
				this.setState({
					users:result
				});
			});
	}
	render() {                            
		return (
            <div>
                {
						this.state.users.map(function(item, key) {             
						return (<Card body>
                            <CardTitle>{item.id}</CardTitle>
                            <CardText>{item.title}</CardText>
                            <CardImg bottom width="45%" src={item.thumbnailUrl} alt="Card image cap" />
                            </Card>  
								  
								
							)
						})
					}
      
     
    </div>
			
		)
	}
}

export default Apicomponent2;